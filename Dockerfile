FROM python:3-alpine

COPY /src/requirements.txt /tmp

RUN pip3 install -r /tmp/requirements.txt && \
    pip3 install ptvsd

WORKDIR /opt/script

COPY /src .

CMD [ "python3", "./main.py" ]