from bs4 import BeautifulSoup
import requests
import pymongo
import re
import os
import threading

MEDIUM_URL = "http://medium.com"

host = os.environ.get("MONGODB_HOST")
user = os.environ.get("MONGODB_USER")
pwd = os.environ.get("MONGODB_PASS")

client = pymongo.MongoClient(f"mongodb://{user}:{pwd}@{host}/")
db = client.get_database(name="articles")
article_collection = db.get_collection("Articles")

class DownloadThread (threading.Thread):
  def __init__(self, article: str):
    threading.Thread.__init__(self)

    self.article = article
    self.url = "%s/p/%s" % (MEDIUM_URL, article)

  def run(self):
    print(f"Getting article {self.article}...")
    res = requests.get(self.url)

    content = BeautifulSoup(res.text, features="html.parser").prettify()
    f = open("/opt/work/Input Files/%s.html" % self.article, 'w')
    f.write(content)
    f.close()
  

def getArticles():
  print("Getting articles index...")
  res = requests.get("%s/topic/startups" % MEDIUM_URL)
  soup = BeautifulSoup(res.text, features="html.parser")

  articles = []

  for h3 in soup.select("h3"):
    title = h3.get_text()

    uri = h3.select_one("a")["href"]
    match = re.match(r"/p/(.*?)\?", uri)

    if not match:
      continue

    article_id = match[1]

    if article_collection.count_documents({ "articleId": article_id }):
      continue

    article_collection.insert_one({
      "articleId": article_id,
      "title": title
    })

    articles.append(article_id)

  return articles


if __name__ == "__main__":
  threads = []

  for article in getArticles():
    thread = DownloadThread(article)
    thread.start()

    threads.append(thread)

  for t in threads:
    t.join()