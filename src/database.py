import pymongo

class Database:
  """
  A DAO for a MongoDB database
  """

  def __init__ (self, connString: str):
    self.client = pymongo.MongoClient(connString)

    database = self.client.get_database(name="files")

    self.parsedFiles = database.get_collection("ParsedFiles")
    self.errorFiles = database.get_collection("ErrorFiles")
    self.stopWords = database.get_collection("StopWords")

    self._seedStopWords()

  def _seedStopWords(self):
    """
    Seed the StopWords collection with values from a CSV file
    """

    if not self.stopWords.count_documents({}):
      file = open('./seedstopwords.csv', 'r')

      for word in file.readlines():
        if not len(word):
          continue

        self.stopWords.insert_one({
          "word": word.strip()
        })

      file.close()

  def getStopWords(self):
    """
    Get a list of all of the stop words
    """

    return [word["word"] for word in self.stopWords.find({})]

  def getAllParsedFiles(self):
    """
    Get all documents from the Parsed Files collection
    """

    return self.parsedFiles.find({})

  def addParsedFile(self, content: dict):
    """
    Add a new Parsed Files document
    """

    return self.parsedFiles.insert_one(content)

  def addErroredFile(self, content: dict):
    """
    Add a new Error File document
    """

    return self.errorFiles.insert_one(content)
  
  def updateRelationships(self, doc_id, relationships: dict):
    """
    Update a documents relationships in the ParsedFiles collection
    """

    return self.parsedFiles.update_one({
      "_id": doc_id,
    }, {
      "$set": {
        "relationships": relationships
      }
    })

  def fileExists(self, md5: str):
    """
    Determine if a file exists by checking the files MD5 hash against both the
    ParsedFiles and ErrorFiles tables.
    """

    has_parsed_file = bool(self.parsedFiles.count({
      "md5": md5
    }))

    has_errored_file = bool(self.errorFiles.count({
      "md5": md5
    }))

    return has_parsed_file or has_errored_file