import re
import hashlib

from bs4 import BeautifulSoup

class Document:
  """
  An HTML document
  """

  def __init__  (self, path: str, stop_words: list):
    file = open(path, 'r')

    self.content = ""
    self.stop_words = stop_words

    for line in file.readlines():
      self.content += line

    self.soup = BeautifulSoup(self.content, features="html.parser")

    for tag in self.soup(["script", "style"]):
      tag.extract()

  def getHash(self):
    """
    Return the MD5 hash of a document's contents
    """

    return hashlib.md5(self.content.encode("utf-8")).hexdigest()

  def getHeadings(self):
    """
    Get a list of all headings and subheadings
    """

    headings = []

    for heading in self.soup(["h1", "h2", "h3", "h4", "h5", "h6"]):
      headings.append(heading.get_text().strip())

    return headings

  def getTitle(self):
    """
    Get the <title /> of the document, or "NO TITLE" if none is found
    """

    titles = self.soup(["title"])

    if not len(titles):
      return "NO TITLE"

    return titles[0].get_text().strip()

  def getWordCounts(self):
    """
    Get a count of all words, lowercased, that are 2+ charactrers long
    """
    
    words = {}

    for word in self.soup.get_text().split():
      stripped = re.sub("[^A-Za-z]", "", word).lower()

      if len(stripped) < 2:
        continue

      if not stripped in words:
        words[stripped] = 1
      else:
        words[stripped] += 1
      
    return words

  def getScoreInsertIndex(self, score: int, scores: list, start: int = 0, end: int = None):
    """
    Use a binary tree approach to place the score in the list in
    the order of highest to lowest score.
    """

    if (not len(scores) or score > scores[0]["score"]):
      return 0

    if not end:
      end = len(scores)

    if end - start <= 1:
      return end

    midpoint = int((start + end) / 2)

    if (score > scores[midpoint]["score"]):
      return self.getScoreInsertIndex(score, scores, start, midpoint)
    elif (score < scores[midpoint]["score"]):
      return self.getScoreInsertIndex(score, scores, midpoint, end)
    else:
      return midpoint
    
  def getImportantWords (self):
    """
    Get the top 10 most important words from the document along with their
    word count, rank, and score. Uses the following algorith to calculate
    their score:

    (Length of Word * Occurrance) + (Occurance in any header * 50)
    """

    headings = self.getHeadings()
    word_counts = self.getWordCounts()
    heading_string = ' '.join(headings).lower()

    scores = []

    for word in word_counts.keys():
      word = word.strip().lower()

      if word in self.stop_words:
        continue

      score = (len(word) * word_counts[word]) + (heading_string.count(word) * 50)

      word_index = self.getScoreInsertIndex(score=score, scores=scores)

      scores.insert(word_index, {
        "word": word,
        "score": score,
        "count": word_counts[word]
      })

    words = {}

    for idx, score in enumerate(scores[:10]):
      words[score["word"]] = {
        "rank": idx,
        "score": score["score"],
        "count": score["count"]
      }

    return words