import os
import logging
import time

import database
import scanner

def createLogger():
  fmt = "(%(asctime)s) %(levelname)s: %(message)s"

  levels = {
    "DEBUG": logging.DEBUG,
    "INFO": logging.INFO,
    "WARNING": logging.WARNING,
    "ERROR": logging.ERROR,
    "CRITICAL": logging.CRITICAL
  }

  level = levels[os.environ.get("LOG_LEVEL", "WARNING")]

  logging.basicConfig(level=level, format=fmt)

  return logging.getLogger('root')

def createMongoDBConnString():
  mongodb_user = os.environ.get("MONGODB_USER")
  mongodb_pass = os.environ.get("MONGODB_PASS")
  mongodb_host = os.environ.get("MONGODB_HOST")
  mongodb_port = os.environ.get("MONGODB_PORT", "27017")
  
  if not mongodb_user or not mongodb_pass:
    raise BaseException("You must supply the MONGODB_USER, MONGODB_PASS, and MONGODB_HOST environment variables")

  return f"mongodb://{mongodb_user}:{mongodb_pass}@{mongodb_host}:{mongodb_port}/"

if __name__ == "__main__":
  if (os.environ.get('DEBUGGING') == "true"):
    # If the debugging flag is passed, print Start Debugging so the vscode task
    # listener will know the script is running, and wait 1 second to give the
    # debugger time to attach to the proccess
    print("Start debugging")
    time.sleep(1)

  log = createLogger()

  database = database.Database(connString = createMongoDBConnString())
  interval = os.environ.get("INTERVAL", (10 * 60))

  while True:
    log.info("Starting scanner...")
    scan = scanner.Scanner("/opt/work", database)

    scan.parseFiles()
    scan.getRelationships()

    log.info(f"Scanner finished. Waiting {interval} seconds...")
    time.sleep(int(interval))