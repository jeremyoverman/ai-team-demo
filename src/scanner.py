import os
import sys
import logging
import threading

import document
import database

log = logging.getLogger('root')

class ParseThread (threading.Thread):
  """
  A thread for reading, parsing, and storing HTML file's important words
  and other general metrics.
  """

  def __init__(self, path: str, name: str, db: database.Database):
    threading.Thread.__init__(self)

    self.path = path
    self.db = db
    self.name = name

    self.input_path = os.path.join(self.path, "Input Files")
    self.parsed_path = os.path.join(self.path, "Clean Files")
    self.errored_path = os.path.join(self.path, "Error Files")

  def _copyFile(self, content: str, output: str):
    """
    Copy a files contents to the output directory overwriting
    existing files.
    """

    file = open(output, 'w')

    file.write(content)

    file.close()

  def _handleError(self, name: str, path: str, err: Exception):
    """
    Insert the error into the ErrorFiles collection and move it to the
    Error Files directory.
    """

    log.warn(f"Unable to parse {name}: {str(err)}")

    result = self.db.addErroredFile({
      "name": name,
      "error": str(err)
    })

    os.rename(path, os.path.join(self.errored_path, str(result.inserted_id)))

  def run(self):
    """
    The base run method for the thread.
    """

    path = os.path.join(self.input_path, self.name)

    try:
      doc = document.Document(path, self.db.getStopWords())
    except Exception as e:
      return self._handleError(self.name, path, e)

    md5 = doc.getHash()

    if self.db.fileExists(md5):
      log.debug("Skipping file %s" % self.name)
      return

    log.info("Parsing file: %s" % self.name)
     
    result = self.db.addParsedFile({
      "file": self.name,
      "md5": md5,
      "title": doc.getTitle(),
      "headings": doc.getHeadings(),
      "words": doc.getImportantWords()
    })

    self._copyFile(doc.content, os.path.join(self.parsed_path, str(result.inserted_id)))

class Scanner:
  """
  A class to scan a directory of HTML files and parse out important words
  along with other general metrics.
  """

  def __init__(self, path: str, db: database.Database):
    self.path = path
    self.db = db

    self._createDirectoriesIfNotExists()

  def _createDirectoriesIfNotExists(self):
    for d in ["Input Files", "Clean Files", "Error Files"]:
      os.makedirs(os.path.join(self.path, d), exist_ok=True)

  def parseFiles(self):
    """
    Read the input directory and spin off a parsing thread for each file
    """

    threads = []

    for file in os.listdir(os.path.join(self.path, "Input Files")):
      thread = ParseThread(self.path, file, self.db)
      thread.start()

      threads.append(thread)

    for thread in threads:
      thread.join()

  def getGlobalWordMap(self):
    """
    Get a map of words with their relationship to other documents. Returns a
    format like so:

    {
      "word1": (document_id, word_count),
      ...
    }
    """

    all_docs = self.db.getAllParsedFiles()

    word_map = {}

    for doc in all_docs:
      for word in doc["words"]:
        if not word in word_map:
          word_map[word] = []

        word_map[word].append((doc.get("_id"), doc["words"][word]["count"]))

    return word_map


  def getRelationships(self):
    """
    Iterate through all words in all documents and find/store while other
    documents contain those same words, and the how many times that word
    has occurred in the other documents.
    """

    all_docs = self.db.getAllParsedFiles()
    word_map = self.getGlobalWordMap()

    for doc in all_docs:
      relationships = {}

      for word in doc["words"]:
        # Don't self reference this document
        related = list(filter(lambda r: r[0] != doc.get("_id"), word_map[word]))

        relationships[word] = related

      self.db.updateRelationships(doc.get("_id"), relationships)