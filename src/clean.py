import os
import pymongo

PARSED_FILES = "/opt/work/Clean Files"
ERRORED_FILES = "/opt/work/Error Files"

host = os.environ.get("MONGODB_HOST")
user = os.environ.get("MONGODB_USER")
pwd = os.environ.get("MONGODB_PASS")

client = pymongo.MongoClient(f"mongodb://{user}:{pwd}@{host}/")
db = client.get_database("files")

def removeParsedFiles():
  for f in os.listdir(PARSED_FILES):
    os.remove(os.path.join(PARSED_FILES, f))

  db.drop_collection("ParsedFiles")

def removeErroredFiles():
  for f in os.listdir(ERRORED_FILES):
    os.remove(os.path.join(ERRORED_FILES, f))

  db.drop_collection("ErroredFiles")

if __name__ == "__main__":
  removeErroredFiles()
  removeParsedFiles()