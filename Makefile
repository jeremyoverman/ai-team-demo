default: build

user_id = `id -u $(USER)`
host = 'ec2-34-207-195-197.compute-1.amazonaws.com'

build:
	docker-compose build

shell:
	docker-compose run -u $(user_id) --rm scanner sh

run:
	docker-compose run -u $(user_id) --rm scanner

clean:
	docker-compose run -u $(user_id) --rm scanner python3 ./clean.py

update:
	docker-compose run -u $(user_id) --rm scanner python3 ./mediumImporter.py

push: build
	docker-compose push scanner
	scp ./docker-compose.yaml centos@$(host):/opt
	ssh centos@$(host) "docker-compose -f /opt/docker-compose.yaml pull scanner"
	ssh centos@$(host) "docker-compose -f /opt/docker-compose.yaml up"

debug:
	docker-compose run \
		-u $(user_id) \
		-p 5678:5678 \
		-e DEBUGGING=true \
		--rm scanner \
		python -m ptvsd \
			--host 0.0.0.0 \
			--port 5678 \
			./main.py